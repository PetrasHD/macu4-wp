<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( is_user_logged_in() ) {
	return;
}

?>

<div class="shape-layout">
    <div class="shape-layout__shape"></div>
    <div class="shape-layout__content">
        <div class="authentication-form">
            <a href="<?php echo home_url(); ?>" class="authentication-form__logo-link">
                <img src="<?php echo get_field('page_logo', 'option')['url']; ?>"
                    alt="<?php echo get_field('page_logo', 'option')['alt']; ?>" class="authentication-form__logo">
            </a>
            <h1 class="authentication-form__title">
                <?php esc_html_e( 'Sign in For Professionals', 'woocommerce' ); ?>
            </h1>
            <form class="woocommerce-form login authentication-form__form" method="post"
                <?php echo ( $hidden ) ? 'style="display:none;"' : ''; ?>>

                <?php do_action( 'woocommerce_login_form_start' ); ?>

                <?php echo ( $message ) ? wpautop( wptexturize( $message ) ) : ''; // @codingStandardsIgnoreLine ?>


                <div class="authentication-form__form-fields">
                    <div class="swiss-input swiss-input--full">
                        <label for="username"
                            class="swiss-input__label"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span
                                class="required">*</span></label>
                        <input type="text" name="username" id="username" autocomplete="username"
                            class="input-text swiss-input__input"
                            placeholder="<?php esc_html_e( 'Email address', 'woocommerce' ); ?>" />
                    </div>

                    <div class="swiss-input swiss-input--full">
                        <label for="password"
                            class="swiss-input__label"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span
                                class="required">*</span></label>
                        <input type="password" name="password" id="password" autocomplete="current-password"
                            class="input-text swiss-input__input"
                            placeholder="<?php esc_html_e( 'Password', 'woocommerce' ); ?>" />
                    </div>
                </div>

                <?php do_action( 'woocommerce_login_form' ); ?>


                <label
                    class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
                    <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme"
                        type="checkbox" id="rememberme" value="forever" />
                    <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
                </label>
                <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                <input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ); ?>" />

                <button type="submit" name="login" class="btn btn--gold authentication-form__confirm-btn"
                    value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>">
                    <?php esc_html_e( 'Login', 'woocommerce' ); ?>
                </button>

                <p class="lost_password">
                    <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"
                        class="authentication-form__right-link"><?php esc_html_e( 'Forgot your password?', 'woocommerce' ); ?></a>
                </p>

                <div class="clear"></div>

                <?php do_action( 'woocommerce_login_form_end' ); ?>

            </form>

        </div>
    </div>
</div>
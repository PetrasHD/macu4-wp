<?php
/**
 * Shop breadcrumb
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/breadcrumb.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! empty( $breadcrumb ) ) : ?>
<ul class="breadcrumbs">
    <?php foreach ( $breadcrumb as $key => $crumb ) : ?>
    <li class="breadcrumbs__item">
        <?php if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 && esc_html( $crumb[0] ) !== 'Home' && esc_html( $crumb[0] ) !== 'My account' && esc_html( $crumb[0] ) !== 'Checkout' ): ?>
        <a href="<?php echo esc_url( $crumb[1] ) ?>" class="breadcrumbs__link"><?php echo esc_html( $crumb[0] ) ?></a>
        <span class="breadcrumbs__arrow">
            <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M4.58579 6L0.292894 1.70711C-0.09763 1.31658 -0.09763 0.683417 0.292894 0.292893C0.683419 -0.0976311 1.31658 -0.097631 1.70711 0.292893L6.70711 5.29289C7.09763 5.68342 7.09763 6.31658 6.70711 6.70711L1.70711 11.7071C1.31658 12.0976 0.683418 12.0976 0.292893 11.7071C-0.0976309 11.3166 -0.0976309 10.6834 0.292894 10.2929L4.58579 6Z"
                    fill="#C7CBCF" />
            </svg>
        </span>
        <?php elseif(esc_html( $crumb[0] ) !== 'Home' && esc_html( $crumb[0] ) !== 'My account' && esc_html( $crumb[0] ) !== 'Checkout'): ?>
        <?php if(strpos(esc_html( $crumb[0] ), 'Orders') !== false) : ?>
        <div class="breadcrumbs__link">Projects</div>
        <?php else: ?>
        <div class="breadcrumbs__link"><?php echo esc_html( $crumb[0] ) ?></div>
        <?php endif; ?>
        <?php endif; ?>
    </li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>
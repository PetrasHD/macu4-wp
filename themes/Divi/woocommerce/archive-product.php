<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

global $woocommerce_loop;

$category = (isset($_GET['product-categories']) && !empty($_GET['product-categories'])) ? $_GET['product-categories'] : 'all';

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked  - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

if (is_shop()) :
?>
<div class="full-back-text-block">
    <div class="row">
        <h1 class="full-back-text-block__title"><?php the_field('shop_title', 'option'); ?></h1>
        <?php $shopDescription = get_field('shop_description', 'option'); 
		if (!empty($shopDescription)):?>
        <p class="full-back-text-block__text"><?= $shopDescription; ?></p>
        <?php endif; ?>
    </div>
</div>
<?php
endif;

if(!function_exists('wc_get_products')) {
    return;
}

$paged                   = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
$ordering                = WC()->query->get_catalog_ordering_args();
$formedOrdering          = explode(' ', $ordering['orderby']);
$ordering['orderby']     = array_shift($formedOrdering);
$ordering['orderby']     = stristr($ordering['orderby'], 'price') ? 'meta_value_num' : $ordering['orderby'];
$products_per_page       = apply_filters('loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page());

$catArray = array();
if (!empty($category) && $category !== 'all') {
    array_push ( $catArray, $category );
}

$categorized_products  = wc_get_products(array(
'status'               => 'publish',
'limit'                => $products_per_page,
'page'                 => $paged,
'paginate'             => true,
'orderby'              => 'title',
'order'                => 'ASC',
'category'             => $catArray,
));

wc_set_loop_prop('current_page', $paged);
wc_set_loop_prop('is_paginated', wc_string_to_bool(true));
wc_set_loop_prop('page_template', get_page_template_slug());
wc_set_loop_prop('per_page', $products_per_page);
wc_set_loop_prop('total', $categorized_products->total);
wc_set_loop_prop('total_pages', $categorized_products->max_num_pages);

if ( $categorized_products ) : 
echo '<div class="woocommerce-notices-wrapper">';
wc_print_notices();
echo '</div>';
?>
<div class="row">
    <div class="product-catalog">
        <div class="product-catalog__controls">
            <?php woocommerce_result_count(); ?>

            <?php
                $product_categories = get_terms('product_cat');
            ?>

            <form action="" class="select-with-label js-custom-select">
                <label for="product-categories" class="select-with-label__label">Product categories:</label>
                <select name="product-categories" id="product-categories" class="select-with-label__select"
                    onchange="this.form.submit()">
                    <option value="all">All</option>
                    <?php foreach($product_categories as $cat): ?>
                    <option value="<?php echo trim($cat->slug); ?>"
                        <?php echo $category == trim($cat->slug) ? 'selected' : ''; ?>><?php echo trim($cat->name); ?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </form>
        </div>
        <?php
	woocommerce_product_loop_start();

    foreach($categorized_products->products as $categorized_product) {
        $post_object = get_post($categorized_product->get_id());        
        setup_postdata($GLOBALS['post'] = $post_object);
        do_action( 'woocommerce_shop_loop' );
        wc_get_template_part('content', 'product');
      }
      wp_reset_postdata();

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' ); ?>
        <?php else :
        /**
        * Hook: woocommerce_no_products_found.
        *
        * @hooked wc_no_products_found - 10
        */
        do_action( 'woocommerce_no_products_found' ); ?>
        <?php endif; ?>

    </div>
</div>

<?php
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

get_footer( 'shop' );
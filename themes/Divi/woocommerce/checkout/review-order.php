<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;
?>
<div id="order_review" class="checkout__products ___woocommerce-checkout-review-order">
	<div class="checkout__products-line checkout__products-heading">
		<?php esc_html_e( 'Product', 'woocommerce' ); ?>
	</div>

	<div class="checkout__products-line checkout__products-heading">
		<?php esc_html_e( 'Subtotal', 'woocommerce' ); ?>
	</div>
	<?php
	do_action( 'woocommerce_review_order_before_cart_contents' );

	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

		if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
			?>

			<div class="checkout__products-line">
				<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
				<span class="js-product-count">
					<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', sprintf( '&times;&nbsp;%s', $cart_item['quantity'] ), $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					<?php echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
				</span>
			</div>

			<div class="checkout__products-line"><?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></div>
			<?php
		}
	}

	do_action( 'woocommerce_review_order_after_cart_contents' );
	?>

</div>
<div class="cart__totals-block">
	<div class="cart__totals-line cart__subtotal-text"><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></div>
	<div class="cart__totals-line cart__subtotal-text"><?php wc_cart_totals_subtotal_html(); ?></div>
	<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
	<div class="cart__totals-line cart__subtotal-text"><?php wc_cart_totals_coupon_label( $coupon ); ?></div>
	<div class="cart__totals-line cart__subtotal-text"><?php wc_cart_totals_coupon_html( $coupon ); ?></div>
<?php endforeach; ?>


<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
	<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>
	<?php wc_cart_totals_shipping_html(); ?>
	<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

	<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
		<div class="cart__totals-line cart__subtotal-text"><?php echo esc_html( $fee->name ); ?></div>
		<div class="cart__totals-line cart__total-text"><?php wc_cart_totals_fee_html( $fee ); ?></div>
	<?php endforeach; ?>
<?php endif; ?>


<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
	<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited ?>
	<div class="cart__totals-line cart__subtotal-text"><?php echo esc_html( $tax->label ); ?></div>
	<div class="cart__totals-line cart__total-text"><?php echo wp_kses_post( $tax->formatted_amount ); ?></div>
<?php endforeach; ?>
<?php else : ?>
	<div class="cart__totals-line cart__subtotal-text"><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></div>
	<div class="cart__totals-line cart__total-text"><?php wc_cart_totals_taxes_total_html(); ?></div>
<?php endif; ?>
<?php endif; ?>

<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

<div class="cart__totals-line cart__total-text"><?php esc_html_e( 'Total', 'woocommerce' ); ?></div>
<div class="cart__totals-line cart__total-text"><?php wc_cart_totals_order_total_html(); ?></div>

<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

</div>
<?php
/**
 * Pay for order form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-pay.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

$checkout = WC()->checkout();
$totals = $order->get_order_item_totals(); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}
?>
<div class="fixed-width-layout">
    <div class="row">
        <h1 class="fixed-width-layout__title">Checkout</h1>
        <form id="order_review" method="post" class="two-column-grid">

            <div class="two-column-grid__left checkout">
                <?php if ( $checkout->get_checkout_fields() ) : ?>

                <?php do_action( 'woocommerce_checkout_billing' ); ?>

                <?php do_action( 'woocommerce_checkout_shipping' ); ?>

                <?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

                <?php endif; ?>
            </div>


            <div class="two-column-grid__right">
                <h3 class="checkout__title"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>

                <div class="checkout__products">
                    <div class="checkout__products-line checkout__products-heading">
                        <?php esc_html_e( 'Product', 'woocommerce' ); ?>
                    </div>
                    <div class="checkout__products-line checkout__products-heading">
                        <?php esc_html_e( 'Total', 'woocommerce' ); ?>
                    </div>

                    <?php if ( count( $order->get_items() ) > 0 ) : ?>
                    <?php foreach ( $order->get_items() as $item_id => $item ) : ?>
                    <?php
                        if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
                            continue;
                        }
                    ?>

                    <div
                        class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?> product-name checkout__products-line">
                        <?php echo apply_filters( 'woocommerce_order_item_name', $item->get_name(), $item, false ); ?>
                        <?php echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times;&nbsp;%s', esc_html( $item->get_quantity() ) ) . '</strong>', $item ); ?>
                    </div>
                    <div
                        class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?> product-subtotal checkout__products-line">
                        <?php echo $order->get_formatted_line_subtotal( $item ); ?>
                    </div>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>

                <?php if ( $totals ) : ?>
                <div class="cart__totals-block">
                    <?php foreach ( $totals as $total ) : ?>
                    <div
                        class="cart__totals-line<?php echo $total['label'] === 'Total:' ? ' cart__total-text' : ''; ?>">
                        <?php echo $total['label']; ?></div>
                    <div
                        class="product-total cart__totals-line<?php echo $total['label'] === 'Total:' ? ' cart__total-text' : ''; ?>">
                        <?php echo $total['value']; ?></div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>

                <div id="payment" class="checkout__payment">
                    <?php if ( $order->needs_payment() ) : ?>
                    <ul class="wc_payment_methods payment_methods methods checkout__payment-methods">
                        <?php
                            if ( ! empty( $available_gateways ) ) {
                                foreach ( $available_gateways as $gateway ) {
                                    wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
                                }
                            } else {
                                echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info checkout__payment-info">' . apply_filters( 'woocommerce_no_available_payment_methods_message', esc_html__( 'Sorry, it seems that there are no available payment methods for your location. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) ) . '</li>'; // @codingStandardsIgnoreLine
                            }
                            ?>
                    </ul>
                    <?php endif; ?>

                    <div class="form-row checkout__cta">
                        <input type="hidden" name="woocommerce_pay" value="1" />

                        <?php wc_get_template( 'checkout/terms.php' ); ?>

                        <?php do_action( 'woocommerce_pay_order_before_submit' ); ?>

                        <?php echo apply_filters( 'woocommerce_pay_order_button_html', '<button type="submit" class="button alt btn btn--green cart__proceed-btn" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine ?>

                        <?php do_action( 'woocommerce_pay_order_after_submit' ); ?>

                        <?php wp_nonce_field( 'woocommerce-pay', 'woocommerce-pay-nonce' ); ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
/**
 * Output a single payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<li class="wc_payment_method payment_method_<?php echo esc_attr( $gateway->id ); ?> checkout__payment-method">
    <label for="payment_method_<?php echo esc_attr( $gateway->id ); ?>" class="swiss-radio">
        <input type="radio" id="payment_method_<?php echo esc_attr( $gateway->id ); ?>" name="payment_method"
            value="<?php echo esc_attr( $gateway->id ); ?>" <?php checked( $gateway->chosen, true ); ?>
            class="input-radio swiss-radio__radio"
            data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>">

        <span class="swiss-radio__label"><?php echo $gateway->get_title(); ?> <?php echo $gateway->get_icon(); ?></span>
    </label>

    <?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
    <div class="payment_box payment_method_<?php echo esc_attr( $gateway->id ); ?> checkout__note"
        <?php if ( ! $gateway->chosen ) : /* phpcs:ignore Squiz.ControlStructures.ControlSignature.NewlineAfterOpenBrace */ ?>style="display:none;"
        <?php endif; /* phpcs:ignore Squiz.ControlStructures.ControlSignature.NewlineAfterOpenBrace */ ?>>
        <?php $gateway->payment_fields(); ?>
    </div>
    <?php endif; ?>
</li>
<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<div id="product-<?php the_ID(); ?>" class="sidebar-layout__main js-sidebar-main">
    <div class="sidebar-layout__controls sidebar-layout__controls--no-sidebar">
        <div class="sidebar-layout__controls-left">
            <?php woocommerce_breadcrumb(); ?>
        </div>
    </div>

    <div class="right-sidebar-layout">
        <div class="right-sidebar-layout__main">
            <?php $attachment_ids = $product->get_gallery_image_ids(); ?>
            <div class="image-carousel">
                <div class="main-image-carousel">
                    <?php foreach( $attachment_ids as $attachment_id ) : ?>
                    <div>
                        <img src="<?php echo wp_get_attachment_url( $attachment_id ); ?>"
                            class="main-image-carousel__img" />
                    </div>
                    <?php endforeach; ?>
                </div>

                <div class="image-carousel-nav">
                    <?php foreach( $attachment_ids as $attachment_id ) : ?>
                    <div>
                        <img src="<?php echo wp_get_attachment_url( $attachment_id ); ?>"
                            class="image-carousel-nav__img" />
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div class="right-sidebar-layout__sidebar">
            <div class="right-sidebar-layout__block">
                <h1 class="right-sidebar-layout__title"><?php echo $product->get_title(); ?></h1>
                <div class="right-sidebar-layout__category">
                    Category: <?php echo wc_get_product_category_list($product->get_id()); ?>
                </div>
                <div class="right-sidebar-layout__price">
                    <?php echo $product->get_price_html(); ?>
                </div>
            </div>
            <?php
            $description = get_the_excerpt();
            if ($description): ?>
            <div class="right-sidebar-layout__block">
                <h2 class="right-sidebar-layout__subtitle">Description</h2>
                <p class="right-sidebar-layout__text">
                    <?= $description ?>
                </p>
            </div>
            <?php endif; ?>

            <?php $project_id = get_query_var( 'project', 0 ); ?>
            <div class="right-sidebar-layout__block">
                <?php if (check_project_by_id($project_id)) : ?>
                <form id="addProduct">
                    <input type="hidden" name="product" value="<?php echo $product->get_id();?>">
                    <input type="hidden" name="project" value="<?php echo $project_id;?>">
                    <input type="hidden" name="item" value="0">
                    <input type="hidden" name="product_type" value="simple">
                    <input type="hidden" name="customer" value="<?php echo get_current_user_id();?>">
                    <button type="submit" class="btn btn--green add_product_button">
                        <span>Add to project</span>
                        <div class="swiss-btn-loader">Loading...</div>
                    </button>
                </form>
                <?php else : ?>
                <div class="right-sidebar-layout__message">
                    <div class="marking-icon marking-icon--red marking-icon--small">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M14.25 16.5H13.5C12.6716 16.5 12 15.8284 12 15V11.25C12 10.8358 11.6642 10.5 11.25 10.5H10.5"
                                stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path
                                d="M11.625 6.75C11.4179 6.75 11.25 6.91789 11.25 7.125C11.25 7.33211 11.4179 7.5 11.625 7.5C11.8321 7.5 12 7.33211 12 7.125C12 6.91789 11.8321 6.75 11.625 6.75V6.75"
                                stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z"
                                stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </div>
                    <div class="right-sidebar-layout__message-text">
                        <a class="" href="<?php echo wc_get_account_endpoint_url( 'orders' ); ?>">To add to project,
                            please
                            select a project and browse products from there.</a>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="popup addproduct_popup" data-swiss-popup="addedProductConfirmation">
    <div class="popup__content">
        <div class="popup__top">
            <h3 class="popup__title">Product added</h3>
            <button type="button" class="popup__close js-close-popup">
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M8.41155 7.00032L13.6886 12.2822C14.079 12.6729 14.0787 13.306 13.688 13.6964C13.2973 14.0867 12.6641 14.0864 12.2738 13.6957L6.99628 8.41348L1.70658 13.6953C1.31577 14.0856 0.682602 14.0851 0.292368 13.6943C-0.0978661 13.3035 -0.0973954 12.6703 0.293419 12.2801L5.58271 6.99863L0.29565 1.70679C-0.094698 1.31609 -0.0944116 0.682921 0.296289 0.292573C0.68699 -0.0977741 1.32016 -0.0974878 1.7105 0.293213L6.99797 5.58547L12.2739 0.317343C12.6648 -0.0728905 13.2979 -0.0724199 13.6881 0.318395C14.0784 0.709209 14.0779 1.34237 13.6871 1.73261L8.41155 7.00032Z"
                        fill="#92929D" />
                </svg>
            </button>
        </div>
        <div class="popup__main">
            <div class="marking-icon marking-icon--green">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M14.25 16.5H13.5C12.6716 16.5 12 15.8284 12 15V11.25C12 10.8358 11.6642 10.5 11.25 10.5H10.5"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path
                        d="M11.625 6.75C11.4179 6.75 11.25 6.91789 11.25 7.125C11.25 7.33211 11.4179 7.5 11.625 7.5C11.8321 7.5 12 7.33211 12 7.125C12 6.91789 11.8321 6.75 11.625 6.75V6.75"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
            </div>
            <div class="popup__info">
                <p class="popup__text">

                </p>
            </div>
        </div>
        <div class="popup__controls">
            <?php $project_url = get_project_view_url($project_id); ?>
            <form action="<?php echo $project_url; ?>">
                <button type="submit" class="btn btn--green">View the project</button>
            </form>
        </div>
    </div>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
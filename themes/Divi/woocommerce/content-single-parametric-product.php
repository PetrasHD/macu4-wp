<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$project_id = get_query_var( 'project', 0 );
$item_id = (isset($_GET['item']) && !empty($_GET['item'])) ? $_GET['item'] : '0';
$item = get_project_item_by_id($project_id, (int)$item_id);
$itemFinished = check_project_parametric_item($item);

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<div class="swiss-logo-overlay">
    <div class="swiss-logo-overlay__content">
        <img src="<?php echo get_field('page_logo', 'option')['url']; ?>"
            alt="<?php echo get_field('page_logo', 'option')['alt']; ?>" class="swiss-logo-overlay__logo">
        <div class="swiss-logo-overlay__loader">Loading...</div>
    </div>
</div>

<div id="product-<?php the_ID(); ?>" class="sidebar-layout__main js-sidebar-main">
    <div class="sidebar-layout__controls sidebar-layout__controls--no-sidebar">
        <div class="sidebar-layout__controls-left">
            <?php woocommerce_breadcrumb(); ?>
        </div>
    </div>

    <div class="right-sidebar-layout">
        <div class="right-sidebar-layout__main u-paddings-0">
            <div id="model-viewer" class="model-viewer">
                <button class="model-viewer__zoom-out" data-zoom="-0.1">
                    <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.5625 9.02856H18.4375" stroke="white" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                    </svg>
                </button>
                <button class="model-viewer__zoom-in" data-zoom="0.1">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.5625 10H18.4375" stroke="white" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M10 1.5625V18.4375" stroke="white" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                    </svg>
                </button>
            </div>
        </div>

        <div class="right-sidebar-layout__sidebar">
            <?php if ($project_id != 0 && $item_id != 0) : ?>
            <div class="right-sidebar-layout__notifications">
                <div class="right-sidebar-layout__status-bar">
                    <?php if ($itemFinished) : ?>
                    <div id="item-status" class="status status--completed">Complete</div>
                    <?php else: ?>
                    <div id="item-status" class="status status--failed">Not complete</div>
                    <?php endif; ?>
                </div>

                <div class="right-sidebar-layout__messages">
                    <div class="right-sidebar-layout__message">
                        <div class="marking-icon marking-icon--green marking-icon--small">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M14.25 16.5H13.5C12.6716 16.5 12 15.8284 12 15V11.25C12 10.8358 11.6642 10.5 11.25 10.5H10.5"
                                    stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                                <path
                                    d="M11.625 6.75C11.4179 6.75 11.25 6.91789 11.25 7.125C11.25 7.33211 11.4179 7.5 11.625 7.5C11.8321 7.5 12 7.33211 12 7.125C12 6.91789 11.8321 6.75 11.625 6.75V6.75"
                                    stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z"
                                    stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </div>
                        <div class="right-sidebar-layout__message-text">
                            Product is added to the <a
                                href="<?php echo wc_get_order( $project_id )->get_view_order_url(); ?>">“Project
                                <?= $project_id ?>”</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <div class="right-sidebar-layout__block">
                <h1 class="right-sidebar-layout__title">
                    <?php echo $product->get_title(); ?>
                </h1>
                <div class="right-sidebar-layout__category">
                    Category: <?php echo wc_get_product_category_list($product->get_id()); ?>
                </div>
                <div class="right-sidebar-layout__price"><?php echo $product->get_price_html(); ?></div>
            </div>

            <?php
            $description = get_the_excerpt();
             if ($description): ?>
            <div class="right-sidebar-layout__block">
                <h2 class="right-sidebar-layout__subtitle">Description</h2>
                <p class="right-sidebar-layout__text">
                    <?= $description ?>
                </p>
            </div>
            <?php endif; ?>

            <?php 
                $videoUrl = get_field( "video_link" );
                if ($videoUrl) :
            ?>
            <div class="right-sidebar-layout__block u-mt-minus1">
                <a href="<?= $videoUrl; ?>" class="right-sidebar-layout__video-btn vp-a">
                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M1 2.5C1 1.67157 1.67157 1 2.5 1H29.5C30.3284 1 31 1.67157 31 2.5V29.5C31 30.3284 30.3284 31 29.5 31H2.5C1.67157 31 1 30.3284 1 29.5V2.5Z"
                            stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M13.6507 20.88C13.2969 21.0564 12.877 21.0372 12.5408 20.8291C12.2046 20.6211 12 20.254 12 19.8586V12.1413C12 11.746 12.2046 11.3788 12.5408 11.1708C12.877 10.9628 13.2969 10.9435 13.6507 11.12L21.3693 14.98C21.7559 15.1729 22.0002 15.5679 22.0002 16C22.0002 16.432 21.7559 16.827 21.3693 17.02L13.6507 20.88Z"
                            stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path d="M1 6H31" stroke="white" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M1 26H31" stroke="white" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                    </svg>
                    <span>Watch video tutorial about how to adjust product parameters
                        correctly.</span>
                </a>
            </div>
            <?php endif; ?>

            <div class="right-sidebar-layout__block">
                <h2 class="right-sidebar-layout__subtitle">Product parameters</h2>
                <div id="model-params-holder" class="right-sidebar-layout__model-params"></div>
                <label class="swiss-checkbox u-mt-4">
                    <input type="checkbox" name="finished" id="finished"
                        <?php echo ($itemFinished) ? ' checked' : ''; ?> class="swiss-checkbox__checkbox" />
                    <span class="swiss-checkbox__mark">
                        <svg width="10" height="8" viewBox="0 0 10 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M2.36023 3.2801C1.96718 2.89211 1.33403 2.89621 0.946044 3.28925C0.558057 3.6823 0.562157 4.31545 0.955202 4.70344L3.49616 7.2117C3.91418 7.62434 4.59615 7.58954 4.96999 7.13648L9.0957 2.13648C9.4472 1.71049 9.38682 1.08021 8.96083 0.728708C8.53484 0.377208 7.90456 0.437592 7.55306 0.863579L4.12315 5.02033L2.36023 3.2801Z"
                                fill="white" />
                        </svg>
                    </span>
                    <span class="swiss-checkbox__label">Configuration for this prosthesis is finished and product can
                        be ordered</span>
                </label>
            </div>

            <div class="right-sidebar-layout__block">
                <?php 
                    if (check_project_by_id($project_id)) : ?>
                <form id="addProduct">
                    <input type="hidden" name="product" value="<?php echo $product->get_id();?>">
                    <input type="hidden" name="project" value="<?php echo $project_id;?>">
                    <input type="hidden" name="item" value="<?php echo $item_id;?>">
                    <input type="hidden" name="product_type" value="parametric">
                    <input type="hidden" name="customer" value="<?php echo get_current_user_id();?>">
                    <input id="shapeDiverData" type="hidden" name="data" value="">
                    <?php if ($item_id == 0): ?>
                    <button type="submit" class="btn btn--green add_product_button">
                        <span>Add to project</span>
                        <div class="swiss-btn-loader">Loading...</div>
                    </button>
                    <?php else: ?>
                    <button type="submit" class="btn btn--dark-grey add_product_button">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M17.8 19H2.2C1.53726 19 1 18.4627 1 17.8V6.1944C1.00014 5.55793 1.25308 4.94758 1.7032 4.4976L4.4968 1.7032C4.94698 1.25289 5.55766 0.999924 6.1944 1H17.8C18.4627 1 19 1.53726 19 2.2V17.8C19 18.4627 18.4627 19 17.8 19Z"
                                stroke="#F7F7F7" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M7 1V5.8C7 6.46274 7.53726 7 8.2 7H14.2C14.8627 7 15.4 6.46274 15.4 5.8V1"
                                stroke="#F7F7F7" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M13 3.40039V4.60039" stroke="#F7F7F7" stroke-width="1.5" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M14.1996 10.5996H5.79961C5.13687 10.5996 4.59961 11.1369 4.59961 11.7996V18.9996H15.3996V11.7996C15.3996 11.1369 14.8624 10.5996 14.1996 10.5996Z"
                                stroke="#F7F7F7" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M7 13H10.6" stroke="#F7F7F7" stroke-width="1.5" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M7 15.4004H13" stroke="#F7F7F7" stroke-width="1.5" stroke-linecap="round"
                                stroke-linejoin="round" />
                        </svg>
                        <span>Save</span>
                        <div class="swiss-btn-loader">Loading...</div>
                    </button>
                    <?php endif; ?>
                </form>
                <?php else : ?>
                <div class="right-sidebar-layout__message">
                    <div class="marking-icon marking-icon--red marking-icon--small">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M14.25 16.5H13.5C12.6716 16.5 12 15.8284 12 15V11.25C12 10.8358 11.6642 10.5 11.25 10.5H10.5"
                                stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path
                                d="M11.625 6.75C11.4179 6.75 11.25 6.91789 11.25 7.125C11.25 7.33211 11.4179 7.5 11.625 7.5C11.8321 7.5 12 7.33211 12 7.125C12 6.91789 11.8321 6.75 11.625 6.75V6.75"
                                stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z"
                                stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </div>
                    <div class="right-sidebar-layout__message-text">
                        <a class="" href="<?php echo wc_get_account_endpoint_url( 'orders' ); ?>">To add to project,
                            please
                            select a project and browse products from there.</a>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="popup addproduct_popup" data-swiss-popup="addedProductConfirmation">
    <div class="popup__content">
        <div class="popup__top">
            <h3 class="popup__title">Product added</h3>
            <button type="button" class="popup__close js-close-popup">
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M8.41155 7.00032L13.6886 12.2822C14.079 12.6729 14.0787 13.306 13.688 13.6964C13.2973 14.0867 12.6641 14.0864 12.2738 13.6957L6.99628 8.41348L1.70658 13.6953C1.31577 14.0856 0.682602 14.0851 0.292368 13.6943C-0.0978661 13.3035 -0.0973954 12.6703 0.293419 12.2801L5.58271 6.99863L0.29565 1.70679C-0.094698 1.31609 -0.0944116 0.682921 0.296289 0.292573C0.68699 -0.0977741 1.32016 -0.0974878 1.7105 0.293213L6.99797 5.58547L12.2739 0.317343C12.6648 -0.0728905 13.2979 -0.0724199 13.6881 0.318395C14.0784 0.709209 14.0779 1.34237 13.6871 1.73261L8.41155 7.00032Z"
                        fill="#92929D" />
                </svg>
            </button>
        </div>
        <div class="popup__main">
            <div class="marking-icon marking-icon--green">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M14.25 16.5H13.5C12.6716 16.5 12 15.8284 12 15V11.25C12 10.8358 11.6642 10.5 11.25 10.5H10.5"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path
                        d="M11.625 6.75C11.4179 6.75 11.25 6.91789 11.25 7.125C11.25 7.33211 11.4179 7.5 11.625 7.5C11.8321 7.5 12 7.33211 12 7.125C12 6.91789 11.8321 6.75 11.625 6.75V6.75"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
            </div>
            <div class="popup__info">
                <p class="popup__text">

                </p>
            </div>
        </div>
        <div class="popup__controls">
            <?php $project_url = get_project_view_url($project_id); ?>
            <form action="<?php echo $project_url; ?>">
                <button type="submit" class="btn btn--green">View the project</button>
            </form>
        </div>
    </div>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
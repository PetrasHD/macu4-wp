<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>

<div <?php wc_product_class( 'product-card', $product ); ?>>

    <?php echo woocommerce_get_product_thumbnail(); ?>
    <div class="product-card__content">
        <div class="product-card__title"><?php echo $product->get_title(); ?></div>
        <div class="product-card__category"><?php echo wc_get_product_category_list($product->get_id()); ?></div>
        <?php 
		/**
		 * Hook: woocommerce_after_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_rating - 5
		 * @hooked woocommerce_template_loop_price - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item_title' );

		$projectId = "";
		if (isset($_GET["project"])) {
			$projectId = "?project=" . trim($_GET["project"]);
		}
		?>
    </div>
    <a href="<?php echo get_the_permalink() . $projectId; ?>" class="btn btn--grey product-card__link">View</a>
</div>
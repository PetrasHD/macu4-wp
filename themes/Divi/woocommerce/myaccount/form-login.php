<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

do_action( 'woocommerce_before_customer_login_form' ); ?>

<div class="shape-layout">
    <div class="shape-layout__shape"></div>
    <div class="shape-layout__content">
        <div class="authentication-form">
            <a href="<?php echo home_url(); ?>" class="authentication-form__logo-link">
                <img src="<?php echo get_field('page_logo', 'option')['url']; ?>"
                    alt="<?php echo get_field('page_logo', 'option')['alt']; ?>" class="authentication-form__logo">
            </a>
            <h1 class="authentication-form__title"><?php esc_html_e( 'Sign in For Professionals', 'woocommerce' ); ?>
            </h1>

            <form class="authentication-form__form" method="post">

                <?php do_action( 'woocommerce_login_form_start' ); ?>

                <div class="authentication-form__form-fields">
                    <div class="swiss-input swiss-input--full">
                        <label for="username"
                            class="swiss-input__label"><?php esc_html_e( 'Email address', 'woocommerce' ); ?></label>
                        <input type="text" name="username" id="username" autocomplete="username"
                            value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>"
                            class="swiss-input__input"
                            placeholder="<?php esc_html_e( 'Username or email address', 'woocommerce' ); ?>" />
                    </div>

                    <div class="swiss-input swiss-input--full">
                        <label for="password"
                            class="swiss-input__label"><?php esc_html_e( 'Password', 'woocommerce' ); ?></label>
                        <input type="password" name="password" id="password" autocomplete="current-password"
                            class="swiss-input__input"
                            placeholder="<?php esc_html_e( 'Password', 'woocommerce' ); ?>" />
                    </div>
                </div>
                <?php do_action( 'woocommerce_login_form' ); ?>

                <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"
                    class="authentication-form__right-link"><?php esc_html_e( 'Forgot your password?', 'woocommerce' ); ?></a>

                <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                <button type="submit" name="login" class="btn btn--gold authentication-form__confirm-btn"
                    value="<?php esc_attr_e( 'Sign in', 'woocommerce' ); ?>">
                    <?php esc_html_e( 'Sign in', 'woocommerce' ); ?>
                </button>

                <div class="authentication-form__note">Don’t have an account? <a
                        href="<?php the_field('professionals_registration_page_link', 'option'); ?>">Click here to
                        register</a>.</div>

                <?php do_action( 'woocommerce_login_form_end' ); ?>
            </form>
        </div>
    </div>
</div>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
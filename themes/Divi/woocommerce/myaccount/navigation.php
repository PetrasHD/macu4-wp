<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>

<div class="sidebar js-sidebar">
    <button class="sidebar__sidebar-trigger js-sidebar-trigger">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
            <path
                d="M11,4 L5.5,4 C4.67157288,4 4,4.67157288 4,5.5 L4,18.5 C4,19.3284271 4.67157288,20 5.5,20 L11,20 L11,4 Z M12,4 L12,20 L18.5,20 C19.3284271,20 20,19.3284271 20,18.5 L20,5.5 C20,4.67157288 19.3284271,4 18.5,4 L12,4 Z M3,5.5 C3,4.11928813 4.11928813,3 5.5,3 L18.5,3 C19.8807119,3 21,4.11928813 21,5.5 L21,18.5 C21,19.8807119 19.8807119,21 18.5,21 L5.5,21 C4.11928813,21 3,19.8807119 3,18.5 L3,5.5 Z M5.5,7 C5.22385763,7 5,6.77614237 5,6.5 C5,6.22385763 5.22385763,6 5.5,6 L9.5,6 C9.77614237,6 10,6.22385763 10,6.5 C10,6.77614237 9.77614237,7 9.5,7 L5.5,7 Z M5.5,9 C5.22385763,9 5,8.77614237 5,8.5 C5,8.22385763 5.22385763,8 5.5,8 L8.5,8 C8.77614237,8 9,8.22385763 9,8.5 C9,8.77614237 8.77614237,9 8.5,9 L5.5,9 Z M5.5,11 C5.22385763,11 5,10.7761424 5,10.5 C5,10.2238576 5.22385763,10 5.5,10 L9.5,10 C9.77614237,10 10,10.2238576 10,10.5 C10,10.7761424 9.77614237,11 9.5,11 L5.5,11 Z M5.5,13 C5.22385763,13 5,12.7761424 5,12.5 C5,12.2238576 5.22385763,12 5.5,12 L8.5,12 C8.77614237,12 9,12.2238576 9,12.5 C9,12.7761424 8.77614237,13 8.5,13 L5.5,13 Z" />
        </svg>
    </button>
    <div class="sidebar__user">
        <div class="sidebar__user-details">
            <div class="sidebar__user-name"><?php echo get_proper_user_name(); ?></div>
            <div class="sidebar__user-specialty">Orthopedic Technician</div>
        </div>
        <button type="button" class="sidebar__user-drop-btn" data-trigger-dropdown="sidebarUserDropdown">
            <svg width="11" height="7" viewBox="0 0 11 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M9.6257 0.333252C10.045 0.333252 10.278 0.818218 10.0161 1.1456L5.72312 6.51188C5.52295 6.76208 5.14241 6.76208 4.94225 6.51188L0.649226 1.1456C0.387321 0.818217 0.620408 0.333252 1.03966 0.333252L9.6257 0.333252Z"
                    fill="#92929D" />
            </svg>
        </button>
        <ul class="swiss-dropdown" data-swiss-dropdown="sidebarUserDropdown">
            <li class="swiss-dropdown__item">
                <a href="<?php echo esc_url( wc_get_account_endpoint_url( 'customer-logout' ) ); ?>"
                    class="swiss-dropdown__link">Logout</a>
            </li>
        </ul>
    </div>
    <nav class="nav-sidebar">
        <div class="nav-sidebar__title">Main</div>
        <ul class="nav-sidebar__list">
            <!-- <li class="nav-sidebar__item">
				<a href="#" class="nav-sidebar__link"><svg width="25" height="20" viewBox="0 0 25 20" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" clip-rule="evenodd" d="M8.5 9.25C9.94975 9.25 11.125 8.07475 11.125 6.625C11.125 5.17525 9.94975 4 8.5 4C7.05025 4 5.875 5.17525 5.875 6.625C5.875 8.07475 7.05025 9.25 8.5 9.25Z" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
					<path fill-rule="evenodd" clip-rule="evenodd" d="M4 14.5C4 12.0147 6.01472 10 8.5 10C10.9853 10 13 12.0147 13 14.5H4Z" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M14.5 7H19" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M14.5 10H20.5" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
					<path fill-rule="evenodd" clip-rule="evenodd" d="M22 1H2.5C1.67157 1 1 1.67157 1 2.5V17.5C1 18.3284 1.67157 19 2.5 19H5.5C5.5 18.1716 6.17157 17.5 7 17.5C7.82843 17.5 8.5 18.1716 8.5 19H16C16 18.1716 16.6716 17.5 17.5 17.5C18.3284 17.5 19 18.1716 19 19H22C22.8284 19 23.5 18.3284 23.5 17.5V2.5C23.5 1.67157 22.8284 1 22 1Z" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
					</svg> Patients</a>
			</li> -->
            <li class="nav-sidebar__item">
                <a href="<?php echo esc_url( wc_get_account_endpoint_url( 'orders' ) ); ?>"
                    class="nav-sidebar__link"><svg width="24" height="22" viewBox="0 0 24 22" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M0.888672 2.5C0.888672 1.67157 1.56024 1 2.38867 1H21.6109C22.4393 1 23.1109 1.67157 23.1109 2.5V19.5C23.1109 20.3284 22.4393 21 21.6109 21H2.38867C1.56024 21 0.888672 20.3284 0.888672 19.5V2.5Z"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path d="M0.888672 6H23.1109" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M20.1476 11H14.2217" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M17.1846 16H14.2217" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M7.55566 10.1665V16.8332" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M4.5918 13.5H10.5177" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                    </svg> Projects</a>
            </li>
            <!-- <li class="nav-sidebar__item">
                <a href="#" class="nav-sidebar__link"><svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path d="M8.07227 6.21875V7.24975" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M4.07227 6.21875V7.24975" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M12.0723 6.21875V7.24975" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M23.2498 20.986L21.7598 18.75V13.5C21.7598 11.5 17.9598 9.361 16.5098 8.25"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path
                            d="M18.4401 14.794L14.7921 11.87C14.2832 11.361 13.458 11.361 12.9491 11.87C12.4402 12.3789 12.4402 13.204 12.9491 13.713L16.4821 17.7V20.2C16.4821 21.381 18.1701 23.245 18.1701 23.245"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path
                            d="M13.5098 23.25H2.13377C1.37476 23.2494 0.759765 22.634 0.759766 21.875V2.125C0.759765 1.366 1.37476 0.750552 2.13377 0.75H15.1338C15.8933 0.75 16.5092 1.36545 16.5098 2.125V13.247"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path
                            d="M10.5098 14.25H4.50977C4.09555 14.25 3.75977 14.5858 3.75977 15V19.5C3.75977 19.9142 4.09555 20.25 4.50977 20.25H13.5098"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path d="M3.75977 17.25H12.0098" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M8.25977 14.25V20.25" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                    </svg> Invoices</a>
            </li> -->
            <li class="nav-sidebar__item">
                <a href="<?php echo esc_url( wc_get_account_endpoint_url( 'edit-address' ) ); ?>"
                    class="nav-sidebar__link"><svg width="24" height="25" viewBox="0 0 24 25" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M1.5 8.45001C1.5 7.62158 2.17157 6.95001 3 6.95001H21C21.8284 6.95001 22.5 7.62159 22.5 8.45001V21.95C22.5 22.7784 21.8284 23.45 21 23.45H3C2.17157 23.45 1.5 22.7784 1.5 21.95V8.45001Z"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path
                            d="M22.3002 7.70002L19.9002 1.84102C19.659 1.29816 19.1202 0.948755 18.5262 0.950018H5.4752C4.88081 0.948359 4.34151 1.29782 4.1002 1.84102L1.7002 7.70002"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path d="M12 6.95001V0.950012" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M14.25 18.95H18.75" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                    </svg> Billing & Shipping</a>
            </li>
            <li class="nav-sidebar__item">
                <a href="<?php echo esc_url( wc_get_account_endpoint_url( 'edit-account' ) ); ?>"
                    class="nav-sidebar__link"><svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M10.5783 2.64729C10.9414 3.05033 11.4584 3.28043 12.0009 3.28043C12.5433 3.28043 13.0603 3.05033 13.4234 2.64729L14.3489 1.63175C14.8925 1.0302 15.7557 0.835714 16.5047 1.14607C17.2537 1.45643 17.7264 2.20449 17.6852 3.01418L17.6157 4.38389C17.5885 4.92422 17.7911 5.45084 18.1734 5.83366C18.5556 6.21649 19.082 6.41983 19.6223 6.39345L20.992 6.32399C21.8012 6.2841 22.548 6.75726 22.8576 7.50592C23.1672 8.25457 22.9726 9.11701 22.3715 9.66021L21.3521 10.5799C20.9496 10.9435 20.7198 11.4605 20.7198 12.0029C20.7198 12.5453 20.9496 13.0624 21.3521 13.4259L22.3715 14.3456C22.9731 14.8891 23.1676 15.7524 22.8572 16.5014C22.5469 17.2504 21.7988 17.7231 20.9891 17.6818L19.6194 17.6124C19.0779 17.5845 18.5499 17.7875 18.1664 18.1709C17.783 18.5544 17.58 19.0823 17.6079 19.6239L17.6773 20.9936C17.7132 21.799 17.2412 22.5408 16.4965 22.8495C15.7517 23.1581 14.8933 22.9677 14.3489 22.3731L13.4283 21.3546C13.0649 20.9521 12.548 20.7224 12.0058 20.7224C11.4635 20.7224 10.9466 20.9521 10.5832 21.3546L9.65867 22.3731C9.11509 22.9706 8.25525 23.1631 7.50875 22.8545C6.76226 22.546 6.28937 21.8025 6.32636 20.9955L6.3968 19.6258C6.42465 19.0843 6.22169 18.5563 5.83825 18.1729C5.45481 17.7894 4.92683 17.5865 4.38528 17.6143L3.01557 17.6838C2.2062 17.726 1.45783 17.2544 1.14678 16.5059C0.835728 15.7575 1.02932 14.8944 1.6302 14.3505L2.64868 13.4308C3.0512 13.0673 3.28092 12.5502 3.28092 12.0078C3.28092 11.4654 3.0512 10.9483 2.64868 10.5848L1.6302 9.66021C1.03134 9.11698 0.838031 8.25622 1.14711 7.50908C1.45619 6.76193 2.20109 6.2893 3.00872 6.3279L4.37843 6.39737C4.92103 6.42589 5.45021 6.22266 5.83423 5.83827C6.21824 5.45388 6.42096 4.9245 6.39191 4.38194L6.32636 3.01125C6.28848 2.20397 6.76106 1.45968 7.50777 1.15056C8.25448 0.841436 9.11486 1.03392 9.65867 1.63175L10.5783 2.64729Z"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.0003 16.4054C14.4318 16.4054 16.4029 14.4343 16.4029 12.0027C16.4029 9.57123 14.4318 7.6001 12.0003 7.6001C9.56879 7.6001 7.59766 9.57123 7.59766 12.0027C7.59766 14.4343 9.56879 16.4054 12.0003 16.4054Z"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    </svg> Settings</a>
            </li>
        </ul>
    </nav>
</div>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
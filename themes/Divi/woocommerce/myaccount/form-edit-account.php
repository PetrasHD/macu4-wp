<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit; ?>

<div class="sidebar-layout__controls">
    <div class="sidebar-layout__controls-left">
        <?php woocommerce_breadcrumb(); ?>
    </div>
</div>

<div class="sidebar-layout__main-content sidebar-layout__main-content--wrap">
    <div class="common-block">
        <div class="common-block__flex-wrapper">
            <h2 class="common-block__title">Basic information</h2>
        </div>

        <?php do_action( 'woocommerce_before_edit_account_form' ); ?>

        <form class="woocommerce-EditAccountForm edit-account" action="" method="post"
            <?php do_action( 'woocommerce_edit_account_form_tag' ); ?>>

            <?php do_action( 'woocommerce_edit_account_form_start' ); ?>

            <div class="authentication-form__form-fields">
                <div class="swiss-input">
                    <label for="account_first_name" class="swiss-input__label">
                        <?php esc_html_e( 'First name', 'woocommerce' ); ?>&nbsp;
                        <span class="required">*</span>
                    </label>
                    <input type="text" name="account_first_name" id="account_first_name" autocomplete="given-name"
                        class="swiss-input__input" placeholder="<?php esc_html_e( 'First name', 'woocommerce' ); ?>"
                        value="<?php echo esc_attr( $user->first_name ); ?>" />
                </div>
                <div class="swiss-input">
                    <label for="account_last_name" class="swiss-input__label">
                        <?php esc_html_e( 'Last name', 'woocommerce' ); ?>&nbsp;
                        <span class="required">*</span>
                    </label>
                    <input type="text" name="account_last_name" id="account_last_name" autocomplete="family-name"
                        class="swiss-input__input" placeholder="<?php esc_html_e( 'Last name', 'woocommerce' ); ?>"
                        value="<?php echo esc_attr( $user->last_name ); ?>" />
                </div>
                <div class="swiss-input">
                    <label for="account_display_name" class="swiss-input__label">
                        <?php esc_html_e( 'Display name', 'woocommerce' ); ?>&nbsp;
                        <span class="required">*</span>
                    </label>
                    <input type="text" name="account_display_name" id="account_display_name" class="swiss-input__input"
                        placeholder="<?php esc_html_e( 'Display name', 'woocommerce' ); ?>"
                        value="<?php echo esc_attr( $user->display_name ); ?>" />
                    <div class="swiss-input__note">
                        <?php esc_html_e( 'This will be how your name will be displayed in the account section and in reviews', 'woocommerce' ); ?>
                    </div>
                </div>
                <div class="swiss-input">
                    <label for="account_email" class="swiss-input__label">
                        <?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;
                        <span class="required">*</span>
                    </label>
                    <input type="email" name="account_email" id="account_email" autocomplete="email"
                        class="swiss-input__input" placeholder="<?php esc_html_e( 'Email address', 'woocommerce' ); ?>"
                        value="<?php echo esc_attr( $user->user_email ); ?>" />
                </div>

                <fieldset class="common-block__field-set authentication-form__form-fields">
                    <legend class="common-block__title">
                        <?php esc_html_e( 'Password change', 'woocommerce' ); ?>
                    </legend>

                    <div class="swiss-input">
                        <label for="password_current" class="swiss-input__label">
                            <?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?>
                        </label>
                        <input type="password" name="password_current" id="password_current" autocomplete="off"
                            class="swiss-input__input"
                            placeholder="<?php esc_html_e( 'Current password', 'woocommerce' ); ?>" />
                    </div>

                    <div class="swiss-input">
                        <label for="password_1" class="swiss-input__label">
                            <?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?>
                        </label>
                        <input type="password" name="password_1" id="password_1" autocomplete="off"
                            class="swiss-input__input"
                            placeholder="<?php esc_html_e( 'New password', 'woocommerce' ); ?>" />
                    </div>

                    <div class="swiss-input">
                        <label for="password_2" class="swiss-input__label">
                            <?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?>
                        </label>
                        <input type="password" name="password_2" id="password_2" autocomplete="off"
                            class="swiss-input__input"
                            placeholder="<?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?>" />
                    </div>
                </fieldset>

                <?php do_action( 'woocommerce_edit_account_form' ); ?>
            </div>

            <p>
                <?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
                <button type="submit" class="btn btn--green" name="save_account_details"
                    value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
                <input type="hidden" name="action" value="save_account_details" />
            </p>

            <?php do_action( 'woocommerce_edit_account_form_end' ); ?>
        </form>
        <?php do_action( 'woocommerce_after_edit_account_form' ); ?>
    </div>
    <div class="common-block">
        <h2 class="common-block__title">Account management</h2>
        <div class="common-block__wrapper">
            <button type="button" class="btn btn--red" data-swiss-popup-trigger="deleteAccount">
                Delete my account
            </button>
        </div>
    </div>
</div>

<div class="popup" data-swiss-popup="deleteAccount">
    <div class="popup__content">
        <div class="popup__top">
            <h3 class="popup__title">Delete account?</h3>
            <button type="button" class="popup__close js-close-popup">
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M8.41155 7.00032L13.6886 12.2822C14.079 12.6729 14.0787 13.306 13.688 13.6964C13.2973 14.0867 12.6641 14.0864 12.2738 13.6957L6.99628 8.41348L1.70658 13.6953C1.31577 14.0856 0.682602 14.0851 0.292368 13.6943C-0.0978661 13.3035 -0.0973954 12.6703 0.293419 12.2801L5.58271 6.99863L0.29565 1.70679C-0.094698 1.31609 -0.0944116 0.682921 0.296289 0.292573C0.68699 -0.0977741 1.32016 -0.0974878 1.7105 0.293213L6.99797 5.58547L12.2739 0.317343C12.6648 -0.0728905 13.2979 -0.0724199 13.6881 0.318395C14.0784 0.709209 14.0779 1.34237 13.6871 1.73261L8.41155 7.00032Z"
                        fill="#92929D" />
                </svg>
            </button>
        </div>
        <div class="popup__main">
            <div class="marking-icon marking-icon--red">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M14.25 16.5H13.5C12.6716 16.5 12 15.8284 12 15V11.25C12 10.8358 11.6642 10.5 11.25 10.5H10.5"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path
                        d="M11.625 6.75C11.4179 6.75 11.25 6.91789 11.25 7.125C11.25 7.33211 11.4179 7.5 11.625 7.5C11.8321 7.5 12 7.33211 12 7.125C12 6.91789 11.8321 6.75 11.625 6.75V6.75"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
            </div>
            <div class="popup__info">
                <p class="popup__text">Deleting your account will result in deleting all your information, orders,
                    patient information, projects. This action cannot be undone.</p>
            </div>
        </div>
        <div class="popup__controls">
            <button type="button" class="btn btn--white js-close-popup">
                Cancel
            </button>
            <form method="post">
                <button type="submit" name="delete_user_account" class="btn btn--red">
                    Delete
                </button>
            </form>
        </div>
    </div>
</div>
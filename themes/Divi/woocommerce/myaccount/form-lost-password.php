<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.2
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_lost_password_form' );
?>

<div class="shape-layout">
    <div class="shape-layout__shape"></div>
    <div class="shape-layout__content">
        <div class="authentication-form">
			<a href="<?php echo home_url(); ?>" class="authentication-form__logo-link">
				<img src="<?php echo get_field('page_logo', 'option')['url']; ?>" alt="<?php echo get_field('page_logo', 'option')['alt']; ?>" class="authentication-form__logo" >
			</a>
            <h1 class="authentication-form__title"><?php esc_html_e( 'Password reset', 'woocommerce' ); ?></h1>
            <form method="post" class="authentication-form__form">
                <div class="authentication-form__form-fields">
                    <div class="swiss-input swiss-input--full">
                        <label for="user_login" class="swiss-input__label"><?php esc_html_e( 'Username or email', 'woocommerce' ); ?></label>
                        <input type="text" name="user_login" id="user_login" autocomplete="username" class="swiss-input__input" placeholder="Username or email" />
					</div>                
				</div>
                <div class="authentication-form__note authentication-form__note--dark-grey">
					<?php echo apply_filters( 'woocommerce_lost_password_message', esc_html__( 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'woocommerce' ) ); ?>
				</div>
				<?php do_action( 'woocommerce_lostpassword_form' ); ?>
				<input type="hidden" name="wc_reset_password" value="true" />
                <button type="submit" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>" class="btn btn--gold authentication-form__confirm-btn" >
					<?php esc_html_e( 'Reset password', 'woocommerce' ); ?>
				</button>      
				<div class="authentication-form__note">Don’t have an account? <a href="<?php the_field('professionals_registration_page_link', 'option'); ?>">Click here to register</a>.</div>
				<?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>
            </form>
        </div>
    </div>
</div>
<?php
do_action( 'woocommerce_after_lost_password_form' );

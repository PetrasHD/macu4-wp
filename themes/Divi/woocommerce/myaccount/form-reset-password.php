<?php
/**
 * Lost password reset form.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.5
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_reset_password_form' );
?>

<div class="shape-layout">
    <div class="shape-layout__shape"></div>
    <div class="shape-layout__content">
        <div class="authentication-form">
            <a href="<?php echo home_url(); ?>" class="authentication-form__logo-link">
                <img src="<?php echo get_field('page_logo', 'option')['url']; ?>"
                    alt="<?php echo get_field('page_logo', 'option')['alt']; ?>" class="authentication-form__logo">
            </a>
            <h1 class="authentication-form__title">
                <?php echo apply_filters( 'woocommerce_reset_password_message', esc_html__( 'Enter a new password below', 'woocommerce' ) ); ?>
            </h1>

            <form method="post" class="woocommerce-ResetPassword lost_reset_password authentication-form__form">
                <div class="authentication-form__form-fields">
                    <div
                        class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first swiss-input swiss-input--full">
                        <label for="password_1"
                            class="swiss-input__label"><?php esc_html_e( 'New password', 'woocommerce' ); ?>&nbsp;<span
                                class="required">*</span></label>
                        <input type="password" name="password_1" id="password_1" autocomplete="new-password"
                            class="woocommerce-Input woocommerce-Input--text input-text swiss-input__input"
                            placeholder="New password" />
                    </div>
                    <div
                        class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last swiss-input swiss-input--full">
                        <label for="password_2"
                            class="swiss-input__label"><?php esc_html_e( 'Re-enter new password', 'woocommerce' ); ?>&nbsp;<span
                                class="required">*</span></label>
                        <input type="password" name="password_2" id="password_2" autocomplete="new-password"
                            class="woocommerce-Input woocommerce-Input--text input-text swiss-input__input"
                            placeholder="Re-enter new password" />
                    </div>
                </div>

                <input type="hidden" name="reset_key" value="<?php echo esc_attr( $args['key'] ); ?>" />
                <input type="hidden" name="reset_login" value="<?php echo esc_attr( $args['login'] ); ?>" />

                <div class="clear"></div>

                <?php do_action( 'woocommerce_resetpassword_form' ); ?>

                <input type="hidden" name="wc_reset_password" value="true" />

                <button type="submit" class="btn btn--gold authentication-form__confirm-btn"
                    value="<?php esc_attr_e( 'Save', 'woocommerce' ); ?>"><?php esc_html_e( 'Save', 'woocommerce' ); ?></button>

                <?php wp_nonce_field( 'reset_password', 'woocommerce-reset-password-nonce' ); ?>
            </form>
        </div>
    </div>
</div>
<?php
do_action( 'woocommerce_after_reset_password_form' );
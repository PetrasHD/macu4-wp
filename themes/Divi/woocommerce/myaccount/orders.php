<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_account_orders', $has_orders ); ?>

<div class="sidebar-layout__controls">
    <div class="sidebar-layout__controls-left">
        <?php woocommerce_breadcrumb(); ?>
    </div>
    <div class="sidebar-layout__controls-right">
        <button type="button" class="btn btn--gold" data-swiss-popup-trigger="createProject">
            Create now
        </button>
    </div>
</div>

<div class="sidebar-layout__main-content">
    <?php if ( $has_orders ) : ?>
    <div class="projects-table">
        <div class="projects-table__heading">
            <div class="projects-table__column">Project ID</div>
            <div class="projects-table__column">Patient</div>
            <div class="projects-table__column">Products</div>
            <div class="projects-table__column">Status</div>
            <div class="projects-table__column">Date created</div>
            <div class="projects-table__column">Price</div>
        </div>

        <?php foreach ( $customer_orders->orders as $customer_order ) :
			$order = wc_get_order( $customer_order ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
			$item_count = $order->get_item_count() - $order->get_item_count_refunded();
			$patient = get_post( $order->get_meta('ot_patient_post_id')  ); ?>

        <a href="<?php echo esc_url( $order->get_view_order_url() ); ?>" class="projects-table__link">
            <span class="projects-table__cell false">
                <span class="projects-table__label">Project ID</span>
                <?php echo esc_html( $order->get_order_number() ); ?>
            </span>
            <span class="projects-table__cell false">
                <span class="projects-table__label">Patient</span>
                <?php echo $patient->post_title; ?>
            </span>
            <span class="projects-table__cell false">
                <span class="projects-table__label">Products</span>
                <?php echo esc_html( $order->get_item_count() ); ?>
            </span>
            <span class="projects-table__cell">
                <span class="projects-table__label">Status</span>
                <span
                    class="status status--<?php echo esc_attr( $order->get_status() ); ?>"><?php echo esc_attr( $order->get_status() ); ?></span>
            </span>
            <span class="projects-table__cell false">
                <span class="projects-table__label">Date created</span>
                <?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?>
            </span>
            <span class="projects-table__cell false">
                <span class="projects-table__label">Price</span>
                <?php echo get_woocommerce_currency_symbol( $order->get_currency() ) . $order->get_total(); ?>
            </span>
        </a>
        <?php endforeach; ?>

        <div class="pagination">
            <?php
                $orderPerPage = get_field('orders_per_page', 'option');
                if (empty($orderPerPage)) {
                    $orderPerPage = 10;
                }
				$rangeMin = 1;
				$rangeMax = $orderPerPage * $current_page;
				if ($current_page > 1) {
					$rangeMin = ($current_page - 1) * $orderPerPage + 1; 
				}
				if ($customer_orders->total < $orderPerPage * $current_page) {
					$rangeMax = $customer_orders->total;
				}
				$range = $rangeMin . '-' . $rangeMax;
			?>
            <div class="pagination__count">
                <span class="pagination__range"><?php echo $range; ?></span> of
                <span class="pagination__total"><?php echo $customer_orders->total; ?></span> items
            </div>

            <?php do_action( 'woocommerce_before_account_orders_pagination' ); ?>

            <?php if ( 1 < $customer_orders->max_num_pages ) : ?>
            <?php
                $args = array(
                    'base'          => esc_url( wc_get_endpoint_url( 'orders') ) . '%_%',
                    'format'        => '%#%',
                    'total'         => $customer_orders->max_num_pages,
                    'current'       => $current_page,
                    'show_all'      => false,
                    'end_size'      => 3,
                    'mid_size'      => 3,
                    'prev_next'     => true,
                    'prev_text'     => '<svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M2.41421 6L6.70711 10.2929C7.09763 10.6834 7.09763 11.3166 6.70711 11.7071C6.31658 12.0976 5.68342 12.0976 5.29289 11.7071L0.292893 6.70711C-0.0976311 6.31658 -0.0976311 5.68342 0.292893 5.29289L5.29289 0.292893C5.68342 -0.0976311 6.31658 -0.0976311 6.70711 0.292893C7.09763 0.683418 7.09763 1.31658 6.70711 1.70711L2.41421 6Z" fill="#C7CBCF"/>
                                        </svg>',
                    'next_text'     => '<svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M4.58579 6L0.292894 1.70711C-0.09763 1.31658 -0.09763 0.683417 0.292894 0.292893C0.683419 -0.0976311 1.31658 -0.097631 1.70711 0.292893L6.70711 5.29289C7.09763 5.68342 7.09763 6.31658 6.70711 6.70711L1.70711 11.7071C1.31658 12.0976 0.683418 12.0976 0.292893 11.7071C-0.0976309 11.3166 -0.0976309 10.6834 0.292894 10.2929L4.58579 6Z" fill="#C7CBCF"/>
                                        </svg>',
                    'type'          => 'list',
                    'add_args'      => false,
                    'add_fragment'  => ''
                );
                echo paginate_links( $args );
                ?>
            <?php endif; ?>
        </div>
    </div>
    <?php else : ?>
    <div
        class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
        <a class="woocommerce-Button button"
            href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>"><?php esc_html_e( 'Browse products', 'woocommerce' ); ?></a>
        <?php esc_html_e( 'No project has been made yet.', 'woocommerce' ); ?>
    </div>
    <?php endif; ?>
</div>

<div class="popup" data-swiss-popup="createProject">
    <form id="createProject" method="post" class="popup__content">
        <div class="popup__top">
            <h3 class="popup__title">Create new project</h3>
            <button type="button" class="popup__close js-close-popup">
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M8.41155 7.00032L13.6886 12.2822C14.079 12.6729 14.0787 13.306 13.688 13.6964C13.2973 14.0867 12.6641 14.0864 12.2738 13.6957L6.99628 8.41348L1.70658 13.6953C1.31577 14.0856 0.682602 14.0851 0.292368 13.6943C-0.0978661 13.3035 -0.0973954 12.6703 0.293419 12.2801L5.58271 6.99863L0.29565 1.70679C-0.094698 1.31609 -0.0944116 0.682921 0.296289 0.292573C0.68699 -0.0977741 1.32016 -0.0974878 1.7105 0.293213L6.99797 5.58547L12.2739 0.317343C12.6648 -0.0728905 13.2979 -0.0724199 13.6881 0.318395C14.0784 0.709209 14.0779 1.34237 13.6871 1.73261L8.41155 7.00032Z"
                        fill="#92929D" />
                </svg>
            </button>
        </div>
        <div class="popup__main">
            <div class="marking-icon marking-icon--gold">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M14.25 16.5H13.5C12.6716 16.5 12 15.8284 12 15V11.25C12 10.8358 11.6642 10.5 11.25 10.5H10.5"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path
                        d="M11.625 6.75C11.4179 6.75 11.25 6.91789 11.25 7.125C11.25 7.33211 11.4179 7.5 11.625 7.5C11.8321 7.5 12 7.33211 12 7.125C12 6.91789 11.8321 6.75 11.625 6.75V6.75"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
            </div>
            <div class="popup__info">
                <p class="popup__text">Enter your patient’s name to start the project (no special characters are
                    allowed).</p>
                <div id="create-new-project" class="swiss-input swiss-input--full">
                    <label for="patientName" class="swiss-input__label">Name, surname</label>
                    <input type="text" name="patientName" id="patientName" class="swiss-input__input"
                        placeholder="Name, surname" />
                    <div class="swiss-input__error">Error goes here</div>
                </div>
            </div>
        </div>
        <div class="popup__controls">
            <button type="button" class="btn btn--white js-close-popup">
                Cancel
            </button>
            <button type="submit" name="createNewProject" value="Create" class="btn btn--gold">
                <span>Create</span>
                <div class="swiss-btn-loader">Loading...</div>
            </button>
        </div>
    </form>
</div>

<?php do_action( 'woocommerce_after_account_orders', $has_orders ); ?>
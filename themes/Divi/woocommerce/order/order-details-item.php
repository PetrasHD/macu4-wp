<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}

$productID = $product->get_id();
?>

<li>
    <div class="product-card-big">
        <div class="product-card-big__img">
            <img src="<?php echo get_the_post_thumbnail_url($productID); ?>" alt="Product 1">
        </div>
        <div class="product-card-big__content">
            <div class="product-card-big__top">
                <div class="product-card-big__title"><?php echo $product->get_title(); ?></div>
                <?php if ($status === 'pending'): ?>
                <div class="product-card-big__dropdown">
                    <button type="button" class="more-btn" data-trigger-dropdown="product-<?= $item_id ?>-drop">
                        <svg width="18" height="5" viewBox="0 0 18 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M4 2.16437C4 1.05882 3.10457 0.162598 2 0.162598C0.89543 0.162598 0 1.05882 0 2.16437C0 3.26991 0.89543 4.16613 2 4.16613C3.10457 4.16613 4 3.26991 4 2.16437Z"
                                fill="#92929D" />
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M11 2.16437C11 1.05882 10.1046 0.162598 9 0.162598C7.89543 0.162598 7 1.05882 7 2.16437C7 3.26991 7.89543 4.16613 9 4.16613C10.1046 4.16613 11 3.26991 11 2.16437Z"
                                fill="#92929D" />
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M18 2.16437C18 1.05882 17.1046 0.162598 16 0.162598C14.8954 0.162598 14 1.05882 14 2.16437C14 3.26991 14.8954 4.16613 16 4.16613C17.1046 4.16613 18 3.26991 18 2.16437Z"
                                fill="#92929D" />
                        </svg>
                    </button>
                    <ul class="swiss-dropdown" data-swiss-dropdown="product-<?= $item_id ?>-drop">
                        <li class="swiss-dropdown__item">
                            <button type="button" class="swiss-dropdown__btn"
                                data-swiss-popup-trigger="deleteProjectItem<?= $item_id ?>"><svg width="12" height="12"
                                    viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M3.47157 1.7415L3.86361 0.565371C3.94301 0.327171 4.16593 0.166504 4.41701 0.166504H7.56701C7.81809 0.166504 8.04101 0.327171 8.12041 0.565371L8.51245 1.7415H11.242C11.5642 1.7415 11.8253 2.00267 11.8253 2.32484C11.8253 2.647 11.5642 2.90817 11.242 2.90817H10.742L10.3139 10.1859C10.2595 11.1109 9.49354 11.8332 8.56695 11.8332H3.41707C2.49048 11.8332 1.7245 11.1109 1.67009 10.1859L1.24198 2.90817H0.751302C0.429136 2.90817 0.167969 2.647 0.167969 2.32484C0.167969 2.00267 0.429136 1.7415 0.751302 1.7415H3.47157ZM4.70134 1.7415H7.28268L7.14657 1.33317H4.83745L4.70134 1.7415ZM9.57335 2.90817H2.41066L2.83474 10.1174C2.85288 10.4258 3.1082 10.6665 3.41707 10.6665H8.56695C8.87581 10.6665 9.13114 10.4258 9.14928 10.1174L9.57335 2.90817ZM6.98481 4.38845C7.00491 4.06691 7.28186 3.82254 7.6034 3.84264C7.92493 3.86274 8.1693 4.13969 8.14921 4.46122L7.88671 8.66122C7.86661 8.98276 7.58966 9.22713 7.26812 9.20703C6.94658 9.18694 6.70222 8.90999 6.72231 8.58845L6.98481 4.38845ZM5.26171 8.58845C5.2818 8.90999 5.03744 9.18694 4.7159 9.20703C4.39436 9.22713 4.11741 8.98276 4.09731 8.66122L3.83481 4.46122C3.81472 4.13969 4.05908 3.86274 4.38062 3.84264C4.70216 3.82254 4.97911 4.06691 4.99921 4.38845L5.26171 8.58845Z"
                                        fill="#747D88" />
                                </svg>Remove product</button>
                        </li>
                    </ul>
                </div>
                <?php endif; ?>
            </div>

            <div class="product-card-big__config">
                <?php if( $product->is_type( 'parametric' ) ) : ?>
                <?php 
                    if (check_project_item_line($item, $item_id)) {
                        echo 'Configuration<span class="status status--borderless status--completed">Complete</span>';
                    } else {
                        echo 'Configuration<span class="status status--borderless status--failed">Not complete</span>';
                    } ?>
                <?php endif; ?>
            </div>

            <div class="product-card-big__mobile-price">
                <div class="product-card-big__price-label">Price</div>
                <div class="product-card-big__amount">
                    <?php echo $order->get_formatted_line_subtotal( $item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
                </div>
            </div>

            <?php if( $product->is_type( 'parametric' ) && $status === "pending" ) : ?>
            <a href="<?php echo get_permalink( $productID ) ?>?project=<?php echo $order->get_id(); ?>&item=<?= $item_id ?>"
                class="btn btn--gold product-card-big__link">Customize</a>
            <?php endif; ?>
        </div>

        <div class="product-card-big__price">
            <div class="product-card-big__price-label">Price</div>
            <div class="product-card-big__amount">
                <?php echo $order->get_formatted_line_subtotal( $item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
            </div>
        </div>
    </div>
</li>

<div class="popup" data-swiss-popup="deleteProjectItem<?= $item_id ?>">
    <div class="popup__content">
        <div class="popup__top">
            <h3 class="popup__title">Remove product from the project?</h3>
            <button type="button" class="popup__close js-close-popup">
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M8.41155 7.00032L13.6886 12.2822C14.079 12.6729 14.0787 13.306 13.688 13.6964C13.2973 14.0867 12.6641 14.0864 12.2738 13.6957L6.99628 8.41348L1.70658 13.6953C1.31577 14.0856 0.682602 14.0851 0.292368 13.6943C-0.0978661 13.3035 -0.0973954 12.6703 0.293419 12.2801L5.58271 6.99863L0.29565 1.70679C-0.094698 1.31609 -0.0944116 0.682921 0.296289 0.292573C0.68699 -0.0977741 1.32016 -0.0974878 1.7105 0.293213L6.99797 5.58547L12.2739 0.317343C12.6648 -0.0728905 13.2979 -0.0724199 13.6881 0.318395C14.0784 0.709209 14.0779 1.34237 13.6871 1.73261L8.41155 7.00032Z"
                        fill="#92929D" />
                </svg>
            </button>
        </div>
        <div class="popup__main">
            <div class="marking-icon marking-icon--red">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M14.25 16.5H13.5C12.6716 16.5 12 15.8284 12 15V11.25C12 10.8358 11.6642 10.5 11.25 10.5H10.5"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path
                        d="M11.625 6.75C11.4179 6.75 11.25 6.91789 11.25 7.125C11.25 7.33211 11.4179 7.5 11.625 7.5C11.8321 7.5 12 7.33211 12 7.125C12 6.91789 11.8321 6.75 11.625 6.75V6.75"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
            </div>
            <div class="popup__info">
                <p class="popup__text">
                    <?php echo ($product->is_type( 'parametric' )) ? 'Removing a product will delete all it\'s configuration. ' : ''; ?>
                    Do you really want to remove this product from the project?
                </p>
            </div>
        </div>
        <div class="popup__controls">
            <button type="button" class="btn btn--white js-close-popup">
                Cancel
            </button>
            <!-- TODO: If parametric product - delete custom post type with configs -->
            <form method="post">
                <input type="hidden" name="deletefromProject" value="<?php echo $order->get_id() ; ?>">
                <button type="submit" name="deleteProductItem" value="<?= $item_id ?>"
                    class="btn btn--red">Remove</button>
            </form>
        </div>
    </div>
</div>
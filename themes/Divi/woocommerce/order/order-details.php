<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.6.0
 */

defined( 'ABSPATH' ) || exit;

$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited

if ( ! $order ) {
    return;
}

$order_items = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$patient = get_post( $order->get_meta('ot_patient_post_id')  );
$order_status = esc_attr( $order->get_status() );

$order_finished = check_project_finish_state($order->get_id());
$is_order_btn_disabled = is_order_btn_disabled($order_finished, $order_status);
?>

<div class="sidebar-layout__controls">
    <div class="sidebar-layout__controls-left">
        <?php woocommerce_breadcrumb(); ?>
    </div>
    <div class="sidebar-layout__controls-right">
        <div class="status status--<?= $order_status; ?>">
            <?= $order_status; ?>
        </div>
    </div>
</div>

<div class="sidebar-layout__main-content">
    <div class="patient-card">
        <div class="patient-card__top">
            <div class="marking-icon marking-icon--gold">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M7.76923 7.82353C7.76923 5.15957 9.88727 3 12.5 3C15.1127 3 17.2308 5.15957 17.2308 7.82353C17.2308 9.49625 16.3957 10.9701 15.1274 11.8353C17.9737 12.9209 20 15.7192 20 19V21H5V19C5 15.7192 7.02635 12.9209 9.87257 11.8353C8.6043 10.9701 7.76923 9.49625 7.76923 7.82353ZM12.5 10.5294C13.9657 10.5294 15.1538 9.31795 15.1538 7.82353C15.1538 6.32911 13.9657 5.11765 12.5 5.11765C11.0343 5.11765 9.84615 6.32911 9.84615 7.82353C9.84615 9.31795 11.0343 10.5294 12.5 10.5294ZM12.5 13.4706C9.54347 13.4706 7.13953 15.8829 7.07813 18.8824H17.9219C17.8605 15.8829 15.4565 13.4706 12.5 13.4706Z"
                        fill="#F7F7F7" />
                </svg>
            </div>
            <div class="patient-card__name"><?php echo $patient->post_title; ?></div>
        </div>
        <div class="patient-card__info">
            <div class="patient-card__info-row">
                <div class="patient-card__label">Patient ID</div>
                <div class="patient-card__detail"><?php echo $patient->ID; ?></div>
            </div>
        </div>
    </div>

    <div class="project-details">
        <div class="project-details__controls">
            <div class="tab-bar">
                <button type="button" class="tab-bar__tab active" data-swiss-tab-trigger="patientProducts">
                    <svg width="20" height="22" viewBox="0 0 20 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M16.5704 6.0165L15.0917 4.24206V6.0165H16.5704ZM19.9667 7.45284V17.8248C19.9667 20.0386 18.1721 21.8332 15.9583 21.8332H4.25833C2.04459 21.8332 0.25 20.0386 0.25 17.8248V4.17484C0.25 1.9611 2.04459 0.166504 4.25833 0.166504H13.5517C14.1625 0.166504 14.7419 0.43784 15.1329 0.907124L19.4896 6.13512C19.7979 6.50504 19.9667 6.97132 19.9667 7.45284ZM17.8 8.18317H14.9833C13.8465 8.18317 12.925 7.26162 12.925 6.12484V2.33317H4.25833C3.24121 2.33317 2.41667 3.15771 2.41667 4.17484V17.8248C2.41667 18.842 3.24121 19.6665 4.25833 19.6665H15.9583C16.9755 19.6665 17.8 18.842 17.8 17.8248V8.18317ZM4.69476 14.8998H15.5219C15.7031 14.8998 15.85 15.0292 15.85 15.1887V16.7776C15.85 16.9372 15.7031 17.0665 15.5219 17.0665H4.69476C4.51356 17.0665 4.36667 16.9372 4.36667 16.7776V15.1887C4.36667 15.0292 4.51356 14.8998 4.69476 14.8998Z"
                            fill="#9097A0" />
                    </svg>
                    Products
                </button>
                <?php if ( $order_status == 'completed' || $order_status == 'processing' || $order_status == 'refunded') : ?>
                <button type="button" class="tab-bar__tab tab-bar__tab--svg-stroke"
                    data-swiss-tab-trigger="projectInvoice">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8.07227 6.21875V7.24975" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M4.07227 6.21875V7.24975" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M12.0723 6.21875V7.24975" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M23.2498 20.986L21.7598 18.75V13.5C21.7598 11.5 17.9598 9.361 16.5098 8.25"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path
                            d="M18.4401 14.794L14.7921 11.87C14.2832 11.361 13.458 11.361 12.9491 11.87C12.4402 12.3789 12.4402 13.204 12.9491 13.713L16.4821 17.7V20.2C16.4821 21.381 18.1701 23.245 18.1701 23.245"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path
                            d="M13.5098 23.25H2.13377C1.37476 23.2494 0.759765 22.634 0.759766 21.875V2.125C0.759765 1.366 1.37476 0.750552 2.13377 0.75H15.1338C15.8933 0.75 16.5092 1.36545 16.5098 2.125V13.247"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path
                            d="M10.5098 14.25H4.50977C4.09555 14.25 3.75977 14.5858 3.75977 15V19.5C3.75977 19.9142 4.09555 20.25 4.50977 20.25H13.5098"
                            stroke="#9097A0" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path d="M3.75977 17.25H12.0098" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M8.25977 14.25V20.25" stroke="#9097A0" stroke-width="1.5" stroke-linecap="round"
                            stroke-linejoin="round" />
                    </svg>
                    Invoice
                </button>
                <?php endif; ?>
            </div>

            <?php if ($order_status === 'pending' || ($order_status === 'failed' && count($order_items) > 0)) : ?>
            <div class="project-details__dropdown">
                <button type="button" class="more-btn" data-trigger-dropdown="editProject">
                    <svg width="18" height="5" viewBox="0 0 18 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M4 2.16437C4 1.05882 3.10457 0.162598 2 0.162598C0.89543 0.162598 0 1.05882 0 2.16437C0 3.26991 0.89543 4.16613 2 4.16613C3.10457 4.16613 4 3.26991 4 2.16437Z"
                            fill="#92929D" />
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M11 2.16437C11 1.05882 10.1046 0.162598 9 0.162598C7.89543 0.162598 7 1.05882 7 2.16437C7 3.26991 7.89543 4.16613 9 4.16613C10.1046 4.16613 11 3.26991 11 2.16437Z"
                            fill="#92929D" />
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M18 2.16437C18 1.05882 17.1046 0.162598 16 0.162598C14.8954 0.162598 14 1.05882 14 2.16437C14 3.26991 14.8954 4.16613 16 4.16613C17.1046 4.16613 18 3.26991 18 2.16437Z"
                            fill="#92929D" />
                    </svg>
                </button>
                <ul class="swiss-dropdown" data-swiss-dropdown="editProject">
                    <?php if (count($order_items) > 0 && ($order_status === 'pending' || $order_status === 'failed')) : ?>
                    <li class="swiss-dropdown__item">
                        <a <?php echo $is_order_btn_disabled ? '' : 'href="' . esc_url( $order->get_checkout_payment_url()) . '"' ?>
                            class="swiss-dropdown__link <?php echo $is_order_btn_disabled ? ' disabled' : ''; ?>">
                            <svg width="14" height="14" viewBox="0 0 14 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M5.07077 6.57924L6.39559 7.88701L8.8777 4.8789C9.08274 4.63041 9.45041 4.59518 9.6989 4.80023C9.94739 5.00527 9.98262 5.37293 9.77758 5.62142L6.88958 9.12142C6.6715 9.38571 6.27369 9.40601 6.02984 9.1653L4.25117 7.40952C4.0219 7.1832 4.01951 6.81386 4.24583 6.58458C4.47216 6.3553 4.8415 6.35291 5.07077 6.57924Z"
                                    fill="#747D88" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M4.66732 0.583496H9.33398C11.5891 0.583496 13.4173 2.41167 13.4173 4.66683V9.3335C13.4173 11.5887 11.5891 13.4168 9.33398 13.4168H4.66732C2.41215 13.4168 0.583984 11.5887 0.583984 9.3335V4.66683C0.583984 2.41167 2.41215 0.583496 4.66732 0.583496ZM4.66732 1.75016C3.05649 1.75016 1.75065 3.056 1.75065 4.66683V9.3335C1.75065 10.9443 3.05649 12.2502 4.66732 12.2502H9.33398C10.9448 12.2502 12.2507 10.9443 12.2507 9.3335V4.66683C12.2507 3.056 10.9448 1.75016 9.33398 1.75016H4.66732Z"
                                    fill="#747D88" />
                            </svg>Complete project
                            <div class="btn__tooltip">Not all parametric products are completed</div>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if ($order_status === 'pending') : ?>
                    <li class="swiss-dropdown__item">
                        <?php $shopLink = add_query_arg(  array( 'project' => $order->get_order_number()), wc_get_page_permalink( 'shop' ) ); ?>
                        <a href="<?php echo $shopLink;?>" class="swiss-dropdown__link"><svg width="12" height="12"
                                viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M6.58333 5.41667H10.6667C10.9888 5.41667 11.25 5.67783 11.25 6C11.25 6.32217 10.9888 6.58333 10.6667 6.58333H6.58333V10.6667C6.58333 10.9888 6.32217 11.25 6 11.25C5.67783 11.25 5.41667 10.9888 5.41667 10.6667V6.58333H1.33333C1.01117 6.58333 0.75 6.32217 0.75 6C0.75 5.67783 1.01117 5.41667 1.33333 5.41667H5.41667V1.33333C5.41667 1.01117 5.67783 0.75 6 0.75C6.32217 0.75 6.58333 1.01117 6.58333 1.33333V5.41667Z"
                                    fill="#747D88" />
                            </svg>Add products</a>
                    </li>
                    <li class="swiss-dropdown__item">
                        <button type="button" class="swiss-dropdown__btn" data-swiss-popup-trigger="deleteProject"><svg
                                width="12" height="12" viewBox="0 0 12 12" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M3.47157 1.7415L3.86361 0.565371C3.94301 0.327171 4.16593 0.166504 4.41701 0.166504H7.56701C7.81809 0.166504 8.04101 0.327171 8.12041 0.565371L8.51245 1.7415H11.242C11.5642 1.7415 11.8253 2.00267 11.8253 2.32484C11.8253 2.647 11.5642 2.90817 11.242 2.90817H10.742L10.3139 10.1859C10.2595 11.1109 9.49354 11.8332 8.56695 11.8332H3.41707C2.49048 11.8332 1.7245 11.1109 1.67009 10.1859L1.24198 2.90817H0.751302C0.429136 2.90817 0.167969 2.647 0.167969 2.32484C0.167969 2.00267 0.429136 1.7415 0.751302 1.7415H3.47157ZM4.70134 1.7415H7.28268L7.14657 1.33317H4.83745L4.70134 1.7415ZM9.57335 2.90817H2.41066L2.83474 10.1174C2.85288 10.4258 3.1082 10.6665 3.41707 10.6665H8.56695C8.87581 10.6665 9.13114 10.4258 9.14928 10.1174L9.57335 2.90817ZM6.98481 4.38845C7.00491 4.06691 7.28186 3.82254 7.6034 3.84264C7.92493 3.86274 8.1693 4.13969 8.14921 4.46122L7.88671 8.66122C7.86661 8.98276 7.58966 9.22713 7.26812 9.20703C6.94658 9.18694 6.70222 8.90999 6.72231 8.58845L6.98481 4.38845ZM5.26171 8.58845C5.2818 8.90999 5.03744 9.18694 4.7159 9.20703C4.39436 9.22713 4.11741 8.98276 4.09731 8.66122L3.83481 4.46122C3.81472 4.13969 4.05908 3.86274 4.38062 3.84264C4.70216 3.82254 4.97911 4.06691 4.99921 4.38845L5.26171 8.58845Z"
                                    fill="#747D88" />
                            </svg>Delete project</button>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
            <?php endif; ?>
        </div>

        <div class="project-details__tab active" data-swiss-tab="patientProducts">
            <?php if (( $order_finished ) and $order_status == 'completed' and
                ((get_field( 'order_additional_info', $order->get_id())) or 
                    (( get_field( 'order_tracking_info', $order->get_id()))
                        or ( get_field( 'order_tracking_number', $order->get_id()))))) : ?>
            <div class="information">
                <?php if (get_field( 'order_additional_info',$order->get_id())) : ?>
                <div class="information__block">
                    <div class="marking-icon marking-icon--grey">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M14.25 16.5H13.5C12.6716 16.5 12 15.8284 12 15V11.25C12 10.8358 11.6642 10.5 11.25 10.5H10.5"
                                stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path
                                d="M11.625 6.75C11.4179 6.75 11.25 6.91789 11.25 7.125C11.25 7.33211 11.4179 7.5 11.625 7.5C11.8321 7.5 12 7.33211 12 7.125C12 6.91789 11.8321 6.75 11.625 6.75V6.75"
                                stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z"
                                stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </div>
                    <div class="information__text">
                        <?php echo get_field('order_additional_info', $order->get_id()); ?>
                    </div>
                </div>
                <?php endif; ?>

                <?php if ( ( get_field( 'order_tracking_info', $order->get_id())) or ( get_field( 'order_tracking_number', $order->get_id()))) : ?>
                <div class="information__block">
                    <div class="marking-icon marking-icon--grey">
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M1.5 8.44995C1.5 7.62152 2.17157 6.94995 3 6.94995H21C21.8284 6.94995 22.5 7.62152 22.5 8.44995V21.95C22.5 22.7784 21.8284 23.45 21 23.45H3C2.17157 23.45 1.5 22.7784 1.5 21.95V8.44995Z"
                                stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path
                                d="M22.2992 7.69996L19.8992 1.84096C19.658 1.29809 19.1192 0.948694 18.5252 0.949957H5.47422C4.87983 0.948298 4.34054 1.29776 4.09922 1.84096L1.69922 7.69996"
                                stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M12 6.94995V0.949951" stroke="white" stroke-width="1.5" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M14.25 18.95H18.75" stroke="white" stroke-width="1.5" stroke-linecap="round"
                                stroke-linejoin="round" />
                        </svg>
                    </div>
                    <div class="information__text">
                        <?php 
                            echo ( get_field( 'order_tracking_info', $order->get_id())) ? (get_field('order_tracking_info', $order->get_id())) : ''; 
                            echo  ( get_field( 'order_tracking_number', $order->get_id())) ? (get_field('order_tracking_number', $order->get_id())) : ''; 

                            $link = get_field('order_tracking_url',$order->get_id());
                            if( $link ) {
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                echo "</br><a href='" .esc_url( $link_url ). " target=" .esc_attr( $link_target ). "'>".esc_html( $link_title )."</a>";
                            }
                            ?>
                    </div>
                </div>
                <?php endif; ?>

            </div>
            <?php endif; ?>

            <div class="details">
                <div class="details__block">
                    <div class="details__label">Project ID</div>
                    <div class="details__text"><?php echo esc_html( $order->get_order_number() ); ?></div>
                </div>
                <div class="details__block">
                    <div class="details__label">Project created</div>
                    <div class="details__text">
                        <?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></div>
                </div>
                <div class="details__block">
                    <div class="details__label">Products</div>
                    <div class="details__text"><?php echo esc_html( $order->get_item_count() ); ?></div>
                </div>
                <div class="details__block">
                    <div class="details__label">Status</div>
                    <div class="details__text u-capitalize"><?= $order_status; ?></div>
                </div>
                <div class="details__block">
                    <div class="details__label">Project cost</div>
                    <div class="details__text">
                        <?php echo $order->get_formatted_order_total(); ?>
                    </div>
                </div>
                <?php if (wp_kses_post( $order->get_payment_method_title() )): ?>
                <div class="details__block">
                    <div class="details__label"><?php esc_html_e( 'Payment method:', 'woocommerce' ); ?></div>
                    <div class="details__text">
                        <?php echo wp_kses_post( $order->get_payment_method_title() ); ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>

            <div class="project-details__tab-title">Products
                (<span id="products-count"><?php echo esc_html( $order->get_item_count() ); ?></span>)
            </div>
            <ul class="project-details__products-list">
                <?php
                foreach ( $order_items as $item_id => $item ) {
                 $product = $item->get_product();

                 wc_get_template(
                  'order/order-details-item.php',
                  array(
                   'order'              => $order,
                   'item_id'            => $item_id,
                   'item'               => $item,
                   'product'            => $product,
                   'status'             => $order_status,
               )
              );
             }
             ?>
            </ul>

            <div class="project-details__cta">
                <?php
                if ($order_status === 'pending') :
                $shopLink = add_query_arg(  array( 'project' => $order->get_order_number()), wc_get_page_permalink( 'shop' ) );
                ?>
                <a href="<?php echo $shopLink; ?>" class="btn btn--white">
                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M6.58333 5.41667H10.6667C10.9888 5.41667 11.25 5.67783 11.25 6C11.25 6.32217 10.9888 6.58333 10.6667 6.58333H6.58333V10.6667C6.58333 10.9888 6.32217 11.25 6 11.25C5.67783 11.25 5.41667 10.9888 5.41667 10.6667V6.58333H1.33333C1.01117 6.58333 0.75 6.32217 0.75 6C0.75 5.67783 1.01117 5.41667 1.33333 5.41667H5.41667V1.33333C5.41667 1.01117 5.67783 0.75 6 0.75C6.32217 0.75 6.58333 1.01117 6.58333 1.33333V5.41667Z"
                            fill="#747D88" />
                    </svg>
                    Browse products
                </a>
                <?php endif; ?>
                <?php if (count($order_items) > 0 && ($order_status === 'pending' || $order_status === 'failed')) : ?>
                <a <?php echo $is_order_btn_disabled ? '' : 'href="' . esc_url( $order->get_checkout_payment_url()) . '"' ?>
                    class="swiss-link <?php echo $is_order_btn_disabled ? ' disabled' : ' swiss-link--green'; ?>">
                    Order now
                    <div class="btn__tooltip">Not all parametric products are completed</div>
                </a>
                <?php endif; ?>
            </div>
        </div>

        <?php if ($order_finished) : ?>
        <div class="project-details__tab" data-swiss-tab="projectInvoice">
            <div class="invoice">
                <div class="invoice__title">Invoice <span>#<?php echo esc_html( $order->get_order_number() ); ?></span>
                </div>
                <div class="invoice__date"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?>
                </div>
                <div class="invoice__addresses">
                    <div class="invoice__address-left">
                        <div class="invoice__label">Bill to</div>
                        <?php 
                            $billing_full_name  = $order->get_formatted_billing_full_name();
                            $billing_company = $order->get_billing_company();
                            $billing_address_1  = $order->get_billing_address_1();
                            $billing_state      = $order->get_billing_state();
                            $billing_city       = $order->get_billing_city();
                            $billing_postcode   = $order->get_billing_postcode();

                            echo !empty($billing_full_name) ? "<strong>" . $billing_full_name . ",</strong>" : '';
                            echo !empty($billing_company) ? "<br>" . $billing_company : '';
                            echo !empty($billing_address_1) ? "<br>" . $billing_address_1 : '';
                            $city_line  =  ", " . $billing_postcode . " " . $billing_city . ', ' . $billing_state;
                            echo ( strlen($city_line) > 7 ) ? $city_line : '';
                            ?>
                        <br>
                    </div>
                    <div class="invoice__address-right">
                        <?php
                            $store_address     = WC()->countries->get_base_address();
                            $store_city        = WC()->countries->get_base_city();
                            $store_postcode    = WC()->countries->get_base_postcode();
                            $store_country     = WC()->countries->countries[WC()->countries->get_base_country()];
                            
                            echo '<strong>'.get_field('company_name', 'options').',</strong><br>';
                            echo $store_address . ', ' . $store_postcode . ' ' . $store_city . ', ' . $store_country . "<br>";
                            echo get_field('phone_number', 'options'). "<br>";
                            ?>
                    </div>
                </div>
                <div class="invoice__table">
                    <div class="invoice__header">
                        <div class="invoice__column">Item</div>
                        <div class="invoice__column">Price</div>
                        <div class="invoice__column">QTY</div>
                        <div class="invoice__column">Total</div>
                    </div>
                    <?php foreach ( $order->get_items() as $item_id => $item ) : ?>
                    <?php $product = $item->get_product(); ?>
                    <div class="invoice__row">
                        <div class="invoice__cell">
                            <div class="invoice__label">Item</div><?php echo $item->get_name(); ?>
                        </div>
                        <div class="invoice__cell">
                            <div class="invoice__label">Price</div>
                            <?php echo $product->get_price_html(); ?>
                        </div>
                        <div class="invoice__cell">
                            <div class="invoice__label">QTY</div>
                            <?php echo $item->get_quantity(); ?>
                        </div>
                        <div class="invoice__cell strong">
                            <div class="invoice__label">Total</div>
                            <?php echo wc_price($item->get_total()); ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <div class="invoice__row">
                        <div class="invoice__cell">
                            <div class="invoice__label">Item</div>
                            Shipping
                        </div>
                        <div class="invoice__cell">
                            <div class="invoice__label">Price</div>
                            <?php echo $order->get_shipping_to_display(); ?>
                        </div>
                        <div class="invoice__cell">
                            <div class="invoice__label">QTY</div>
                            1
                        </div>
                        <div class="invoice__cell strong">
                            <div class="invoice__label">Total</div>
                            <?php echo $order->get_shipping_to_display(); ?>
                        </div>
                    </div>
                </div>
                <div class="invoice__totals">
                    <?php if ($order->get_discount_total() > 0) : ?>
                    <div class="invoice__totals-block">
                        <div class="invoice__label">DISCOUNT</div>
                        <div class="invoice__amount"><?php echo $order->get_discount_to_display(); ?></div>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="invoice__totals">
                    <?php if ($order->get_total_tax() > 0) : ?>
                    <div class="invoice__totals-block">
                        <div class="invoice__label">TAX</div>
                        <div class="invoice__amount"><?php echo wc_price($order->get_total_tax()); ?></div>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="invoice__totals">
                    <div class="invoice__totals-block">
                        <div class="invoice__label">Subtotal</div>
                        <div class="invoice__amount"><?php echo $order->get_subtotal_to_display(); ?></div>
                    </div>
                    <div class="invoice__totals-block">
                        <div class="invoice__label">Total</div>
                        <div class="invoice__amount"><?php echo $order->get_formatted_order_total(); ?></div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>

<div class="popup" data-swiss-popup="deleteProject">
    <div class="popup__content">
        <div class="popup__top">
            <h3 class="popup__title">Delete project?</h3>
            <button type="button" class="popup__close js-close-popup">
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M8.41155 7.00032L13.6886 12.2822C14.079 12.6729 14.0787 13.306 13.688 13.6964C13.2973 14.0867 12.6641 14.0864 12.2738 13.6957L6.99628 8.41348L1.70658 13.6953C1.31577 14.0856 0.682602 14.0851 0.292368 13.6943C-0.0978661 13.3035 -0.0973954 12.6703 0.293419 12.2801L5.58271 6.99863L0.29565 1.70679C-0.094698 1.31609 -0.0944116 0.682921 0.296289 0.292573C0.68699 -0.0977741 1.32016 -0.0974878 1.7105 0.293213L6.99797 5.58547L12.2739 0.317343C12.6648 -0.0728905 13.2979 -0.0724199 13.6881 0.318395C14.0784 0.709209 14.0779 1.34237 13.6871 1.73261L8.41155 7.00032Z"
                        fill="#92929D" />
                </svg>
            </button>
        </div>
        <div class="popup__main">
            <div class="marking-icon marking-icon--red">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M14.25 16.5H13.5C12.6716 16.5 12 15.8284 12 15V11.25C12 10.8358 11.6642 10.5 11.25 10.5H10.5"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path
                        d="M11.625 6.75C11.4179 6.75 11.25 6.91789 11.25 7.125C11.25 7.33211 11.4179 7.5 11.625 7.5C11.8321 7.5 12 7.33211 12 7.125C12 6.91789 11.8321 6.75 11.625 6.75V6.75"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M12 23.25C18.2132 23.25 23.25 18.2132 23.25 12C23.25 5.7868 18.2132 0.75 12 0.75C5.7868 0.75 0.75 5.7868 0.75 12C0.75 18.2132 5.7868 23.25 12 23.25Z"
                        stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
            </div>
            <div class="popup__info">
                <p class="popup__text">
                    Deleting project will delete all of the configured products that
                    are added in the project. Do you really want to delete this
                    project?
                </p>
            </div>
        </div>
        <form action="<?php echo wc_get_account_endpoint_url('orders');?>" method="post">

            <div class="popup__controls">
                <input type="hidden" name="deleteProject" value="<?php echo $order->get_ID();?>">
                <button type="button" class="btn btn--white js-close-popup">
                    Cancel
                </button>
                <button type="submit" class="btn btn--red">Delete</button>
            </div>
        </form>
    </div>
</div>
<?php
/**
 * Action hook fired after the order details.
 *
 * @since 4.4.0
 * @param WC_Order $order Order data.
 */
do_action( 'woocommerce_after_order_details', $order );